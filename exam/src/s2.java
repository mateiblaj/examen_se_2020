public class s2 {
    public static void main(String[] args) {
        YThread yThread1 = new YThread("YThread1");
        YThread yThread2 = new YThread("YThread2");

        yThread1.start();
        yThread2.start();
    }
}

class YThread extends Thread {

    YThread(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 4; i++) {
            System.out.println(getName() + "-" + i * 10 + "seconds");
            //if this is the last iteration, don't wait another 10 sec to finish the thread
            if(i!=3) {
                try {
                    Thread.sleep(10000); //wait 10 seconds
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println(getName() + " has displayed all 4 messages");
    }
}